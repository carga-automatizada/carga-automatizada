try:
    import pyodbc
    import sys
    import os


except Exception as ImportError:
    print(str(ImportError))
    print("Modulos nao importados corretamente!! ")
    sys.exit()


HOST = os.environ.get('HIVE_HOST')
PORT = os.environ.get('HIVE_PORT')
USER = os.environ.get('HIVE_USER')
PASSWD = os.environ.get('HIVE_PASSWD').rstrip("\n")



cnxnstr = 'Driver={/scripts/driver/libhortonworkshiveodbc64.so};' \
          'Host='+HOST+';' \
          'Port='+PORT+';' \
          'user={'+USER+'}' \
          'pwd={'+PASSWD+'};' \
          'UID={UID};' \
          'SCHEMA={schema};' \
          'AuthMECH=3;' \
          'SSL=1;' \
          'AllowSelfSignedServerCert=1;' \
          'CAIssuedCertNamesMismatch=1;' \
          'ThriftTransport=2;' \
          'HTTPPath=gateway/default/hive;' \
          'HiveServerType=2;' \
          'ServiceDiscoveryMode=0'



try:
    cnxn = pyodbc.connect(cnxnstr, autocommit=True)
    print("Conexao com Hive realizada com sucesso")

except Exception as Errorconnect:
    print("Falha na comunicacao com o banco de dados!!!")
    print(str(Errorconnect))
    sys.exit()
