try:
    import sys
    from neo4j import GraphDatabase
    import os
    import gerando_log

except Exception as ImportError:
    print("Modulos nao importados corretamente!!")
    print(str(ImportError))
    sys.exit()


USER= os.environ.get("NEO4J_USER")
PASSWD = os.environ.get('NEO4J_PASSWD').rstrip("\n")
URI = os.environ.get('NEO4J_CONNECT')
DBS = os.environ.get('NEO4J_DBS')


try:
    graphconn = GraphDatabase.driver(URI, auth=(USER, PASSWD), database=DBS)
    graph = graphconn.session()
    print("Conexao com Neo4j realizada com sucesso")

except Exception as ConectionError:
    print("Erro na conexao com o Neo4j")
    print(str(ConectionError))
    sys.exit()
