try:
    import sys
    import json
    sys.path.insert(2,'.')
    sys.path.insert(1,'/scripts/connection/ossathena_extration')
    sys.path.insert(1,'/scripts/connection/ossathena_import')
    import extracao
    import importacao
    import hive_connection
    import neo4j_connection

except Exception as ImportError:
    print("Modulos nao importados corretamente!! ")
    print(str(ImportError))
    sys.exit()


#VARIÁVEIS DE CONEXÃO COM OS BANCOS (HIVE/NEO4J)

conn=hive_connection.cnxn
graph=neo4j_connection.graph


#EXECUTA O PROGRAMA E CHAMA AS CLASSES E FUNÇÕES RESPECTIVAMENTE, PASSANDO AS VARIÁVEIS NECESSÁRIAS

class Executor():
    def __init__(self, validador, sql_1, csv_1, clean_1, import_1, sql_2, csv_2, clean_2, import_2, conn, graph):

        self.valida = extracao.Validation(validador, sql_1, csv_1, clean_1, import_1, sql_2, csv_2, clean_2, import_2, conn)
        self.importa = importacao.Import(clean_1, import_1, clean_2, import_2, conn, graph)

json_file_path = json.load(open("/scripts/site/conf/ossathena_extration/controle_carga_extration.json"))
json_file = json.dumps(json_file_path)
file = json.loads(json_file)


valida = (file["validador"])
update_validador = (file["update_validador"])
select_validador = (file["select_validador"])

sql_01 = (file["sql_1"])
sql_02 = (file["sql_2"])
csv_01 = (file["csv_1"])
csv_02 = (file["csv_2"])

clean_01 = (file["clean_1"])
clean_02 = (file["clean_2"])
import_01 = (file["import_1"])
import_02 = (file["import_2"])


####### Lendo arquivos de VALIDAÇÃO ##########

validation = open(valida)
validador = validation.read()
validation.close()

validation_update = open(update_validador)
update_status = validation_update.read()
validation_update.close()

status_select = open(select_validador)
select_status = status_select.read()
status_select.close()

########## Lendo arquivos EXTRACAO ##########

sql01 = open(sql_01)
sql_1 = path_geo.read()
sql01.close()

sql02 = open(sql_02)
sql_2 = sql02.read()
sql02.close()


########### Lendo arquivos DATA_IMPORT ##########

detach_table_01 = open(clean_01)
clean_1 = detach_geographic.read()
detach_geographic.close()

detach_resource = open(clean_02)
clean_2 = detach_resource.read()
detach_resource.close()

load_geographic = open(import_01)
import_1 = load_geographic.read()
load_geographic.close()

load_resource = open(import_02)
import_2 = load_resource.read()
load_resource.close()


def main():
    print("Iniciando Aplicacao")
    Executor(validador, sql_1, csv_1, clean_1, import_1, sql_2, csv_2, clean_2, import_2, conn, graph)

if __name__ == "__main__":
    main()
