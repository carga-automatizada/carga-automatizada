try:
    import sys
    sys.path.insert(1,'.')
    import gerando_log
    import os
except Exception as ImportError:
    print("Modulos nao importados corretamente!!")
    print(str(ImportError))
    sys.exit()



class Import():
    def __init__(clean_1, import_1, clean_2, import_2, graph):


        self.log = gerando_log.Log()
        AMBIENTE = os.environ.get("AMBIENTE_DB")

#VERIFICACAO E LIMPEZA DOS NOS

        try:
            self.log.write_log("Realizando limpeza")
            clean_1 = ("%s" %clean_1)
            res_check_geo=graph.run(clean_1) 
            self.log.write_log("Limpeza realizada, tendo como resposta: %s" %res_check_geo)
        except Exception as ClearError:
            self.log.write_log_error("Limpeza nao executada corretamente!")
            self.log.write_log_error(str(ClearError))
            sys.exit()

        try:
            self.log.write_log("Realizando limpeza")
            clean_2 = ("%s" %clean_2)
            res_check_geo=graph.run(clean_2) 
            self.log.write_log("Limpeza realizada, tendo como resposta: %s" %res_check_geo)
        except Exception as ClearError:
            self.log.write_log_error("Limpeza nao executada corretamente!")
            self.log.write_log_error(str(ClearError))
            sys.exit()


#IMPORTACAO DE DADOS NEO4J

        try:
            self.log.write_log("Importando dados...")
            import_1 = ("%s" %import_1)
            res_import_geo=graph.run(import_1) 
            self.log.write_log("Dados importados com sucesso, tendo como resposta: %s" %res_import_geo)
        except Exception as ImportError:
            self.log.write_log_error("Erro na importacao 01")
            self.log.write_log_error(str(ClearError))
            sys.exit()

        try:
            self.log.write_log("Importando dados...")
            import_2 = ("%s" %import_2)
            res_import_geo=graph.run(import_2) 
            self.log.write_log("Dados importados com sucesso, tendo como resposta: %s" %res_import_geo)
        except Exception as ImportError:
            self.log.write_log_error("Erro na importacao 02")
            self.log.write_log_error(str(ClearError))
            sys.exit()

#ATUALIZAÇÃO DE REGISTRO DA TABELA DE STATUS

        try:
            self.log.write_log("Atualizando tabela status..")
            update_status = update_status.replace('database', AMBIENTE)
            select_status = select_status.replace('database', AMBIENTE)
            update = conn.execute(update_status)
            status = conn.execute(select_status).fetchall()
            self.log.write_log("Tabela status atualizada: %s" %status)
        except Exception as readError:
            self.log.write_log_error("Erro na atualizacao da tabela status!!!")
            self.log.write_log_error(str(readError))
            sys.exit()            

        self.log.write_log("Importacao realizada com sucesso!!")
