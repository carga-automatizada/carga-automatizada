import datetime as dt
import logging
from rocketchat_API.rocketchat import RocketChat

rocket = RocketChat(server_url="http://IP_ADDRESS:PORT", user="username", password="passwd")
room = "szcWcjc5BDfeCxt49"


folder_time = dt.datetime.now().strftime("%Y%m%d-%H-%M-%S")
folder = "/scripts/logs/log_"
log = ".log"


#CLASSE QUE TEM FUNÇÕES QUE GERAM LOGS NO MOMENTO QUE SÃO CHAMADAS: LOG DE ERRO, LOG DE ALERTA E LOG DE INFORMAÇÃO

class Log():
    def __init__(self):
        self.folder = folder

    def write_log(self, message):
        logs=folder+folder_time+log
        hora = str(dt.datetime.now())
        rocket.chat_post_message(message, room_id=room)
        logging.basicConfig(filename=logs, filemode='w', level=logging.DEBUG)
        logging.info(message)


    def write_log_noexec(self, message):
        logs=folder+folder_time+log
        hora = str(dt.datetime.now())
        rocket.chat_post_message(message, room_id=room)
        logging.basicConfig(filename=logs, filemode='w', level=logging.DEBUG)
        logging.warning(message)


    def write_log_error(self, message):
        logs=folder+folder_time+log
        hora = str(dt.datetime.now())  
        rocket.chat_post_message(message, room_id=room)
        logging.basicConfig(filename=logs, filemode='w', level=logging.DEBUG)   
        logging.error(message)
