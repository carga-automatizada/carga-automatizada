try:
    import pyodbc
    import pandas as pd
    import sys
    import os
    sys.path.insert(1,'.')
    import gerando_log
    

except Exception as ImportError:
    print(str(ImportError))
    print("Modulos nao importados corretamente!! ")
    sys.exit()

#Classe de validação
#Valida o resgistro da tabela de status
#STATUS = 'HIVE_ATUALIZADO': O PROGRAMA SERÁ EXECUTADO (ATUALIZAÇÃO DO NEO4J)
#STATUS != 'HIVE_ATUALIZADO': O PROGRAMA SERÁ ENCERRADO

class Validation():

    def __init__(self, validador, sql_1, csv_1, sql_2, csv_2, conn):

        self.log = gerando_log.Log()
        AMBIENTE = os.environ.get("AMBIENTE_DB")

        try:
            self.log.write_log("Validando Entidade Site..")
            validador = validador.replace('database',AMBIENTE)
            status = conn.execute(validador).fetchall()
            self.log.write_log("Os dados do Hive se encontram no status: %s" %status)
            if str(status)  == "[('HIVE_ATUALIZADO', )]":
                self.log.write_log("O Neo4j será atualizado")
                self.extrai = Extration(sql_1, csv_1, sql_2, csv_2, conn)
            else:
                self.log.write_log_noexec("O Neo4j se encontra atualizado. O programa será encerrado!")
                sys.exit()
                
        except Exception as readError:
            self.log.write_log_error("Erro na validação da Entidade Site!!!")
            self.log.write_log_error(str(readError))
            sys.exit()

class Extration():
    def __init__(self, sql_1, csv_1, sql_2, csv_2,conn):

        self.log = gerando_log.Log()
        AMBIENTE = os.environ.get("AMBIENTE_DB")

#EXTRACAO DE CSV

        try:
            self.log.write_log("Lendo tabela 1..")
            sql_1 = sql_1.replace('database',AMBIENTE)
            data_geo_site = pd.read_sql("%s" %(sql_1) ,conn)
            self.log.write_log("Tabela 1 lida com sucesso!!")
        except Exception as readError:
            self.log.write_log_error("Erro na leitura da tabela 1!!!")
            self.log.write_log_error(str(readError))
            sys.exit()

        try:
            self.log.write_log("Lendo tabela 2..")
            sql_2 = sql_2.replace('database',AMBIENTE)
            data_resource = pd.read_sql("%s" %(sql_2) ,conn)
            self.log.write_log("Tabela 2 lida com sucesso!!")
        except Exception as readError:
            self.log.write_log_error("Erro na leitura da tabela 2!!!")
            self.log.write_log_error(str(readError))
            sys.exit()

#EXPORTACAO DOS CSVs


        try:
            self.log.write_log("Exportando CSV 1..")
            csv_1.to_csv("%s" %(csv_geo_site),sep="%",index=False, header=True)
            self.log.write_log("CSV 1 exportado com sucesso!!")
        except Exception as writeError:
            self.log.write_log_error("Erro ao gerar o CSV 1!!!")
            self.log.write_log_error(str(writeError))
            sys.exit()

        try:
            self.log.write_log("Exportando CSV 2..")
            csv_2.to_csv("%s" %(csv_resource),sep="%",index=False, header=True)
            self.log.write_log("CSV 2 exportado com sucesso!!")
        except Exception as writeError:
            self.log.write_log_error("Erro ao gerar o 2!!!")
            self.log.write_log_error(str(writeError))
            sys.exit()

        self.log.write_log("Encerrando extracao...")
