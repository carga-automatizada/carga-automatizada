FROM centos:centos8.3.2011

RUN echo "proxy=http://IP_ADDRESS:8080" >> /etc/yum.conf

RUN yum clean all && yum -yv update
RUN yum install python38 -yv
RUN yum install gcc -y 
RUN yum install gcc-c++.x86_64 -y 
RUN yum install unixODBC-devel -y 
RUN yum install python3-devel -y  
RUN yum install git -y
RUN yum clean all
RUN python3 -m pip install --upgrade pip --proxy 'http://IP_ADDRES:8080' && \
    pip3 install pyODBC --proxy 'http://IP_ADDRES:8080' && \   
    pip3 install pyhive py2neo pandas --proxy 'http://IP_ADDRES:8080' && \
    pip3 install neo4j --proxy 'http://IP_ADDRES:8080'

COPY src /scripts
WORKDIR /scripts

ENTRYPOINT ["tail", "-f", "/dev/null"]
